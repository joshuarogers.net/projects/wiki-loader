import { Box, Chip, Fade, Typography } from '@mui/material';
import { orderBy } from 'lodash';
import { styled } from '@mui/material/styles';
import { usePathfinderNavigation } from '@/libs/navigation';
import { useRouter } from 'next/router';

export type CategoryWidgetProps = { categories: string[]; };
export const CategoryWidget = ({ categories }: CategoryWidgetProps) => {
    const { generatePageUrl } = usePathfinderNavigation();
    const router = useRouter();

    const truncate = (input: string) => {
        const words = input.substring(0, 51);
        return words.length <= 50
            ? words
            : words.substring(0, words.lastIndexOf(' ')) + '...';
    };

    if (categories === undefined || categories.length === 0)
        return (<></>);

    const ListItem = styled('li')(({ theme }) => ({
        margin: theme.spacing(0.25)
    }));

    return (<>
        <Typography variant='h6' sx={{ textAlign: 'center' }}>Categories</Typography>
        <Fade in={categories !== null}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center', listStyle: 'none' }}>
                {categories !== null && orderBy(categories, x => x).map(category => (
                    <ListItem key={category}>
                        <Chip label={truncate(category)}
                            variant='filled'
                            onClick={() => router.push(generatePageUrl({ pageId: `Category:${category}` }))} />
                    </ListItem>
                ))}
            </Box>
        </Fade>
    </>);
};
