import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';

import { Box, Switch } from "@mui/material";
import { useContext } from 'react';
import { ColorModeContext } from './colorMode';

export const DarkModeToggle = () => {
    const { isDarkMode, setDarkMode, setLightMode } = useContext(ColorModeContext);
    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked)
            setLightMode();
        else
            setDarkMode();
    }

    return (
        <Box sx={{ height: "50px", display: "flex", alignItems: "center", justifyContent: "center" }}>
            <DarkModeIcon />
            <Switch checked={!isDarkMode} onChange={onChange} />
            <LightModeIcon />
        </Box>
    );
};
