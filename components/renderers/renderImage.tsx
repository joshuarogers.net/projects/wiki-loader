import styled from "@emotion/styled";
import { Box } from "@mui/material";
import { useContext } from "react";
import { ColorModeContext } from "@/components/colorMode";
import { ImageToken } from "@/libs/tokens/tokenTypes";
import { TokenRenderer } from './rendererTypes';

interface WikipediaImageProps {
    url: string;
}

const WikipediaImage = ({url}: WikipediaImageProps) => {
    const { isDarkMode } = useContext(ColorModeContext);

    const imageBackgroundColor = isDarkMode
        ? '#DDD'
        : 'inherit';

    const ImageContainer = styled(Box)`display: flex; justify-content: center;`;
    const Image = styled.img`max-width: 50%; padding: 5px; background-color: ${imageBackgroundColor}`;

    return (
        <ImageContainer>
            <Image src={url} />
        </ImageContainer>
    );
};

export const renderImage: TokenRenderer<ImageToken> = token => (
    <WikipediaImage url={token.url} />
);
