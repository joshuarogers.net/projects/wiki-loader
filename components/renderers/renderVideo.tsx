import { type VideoToken } from "@/libs/tokens/tokenTypes";
import { type TokenRenderer } from "./rendererTypes";

export const renderVideo: TokenRenderer<VideoToken> = token => (
    <video controls width="100%">
        <source src={token.url} type={`video/${token.format}`} />
    </video>
);
