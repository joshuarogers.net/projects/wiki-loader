import { type TextToken, type TextEmphasisToken } from "@/libs/tokens/tokenTypes";
import { type TokenRenderer } from './rendererTypes';

export const renderText: TokenRenderer<TextToken> = token => token.value;
export const renderTextEmphasis: TokenRenderer<TextEmphasisToken> = (token, renderToken) => {
    const outputElements = token.tokens.map(renderToken);
    if (token.emphasisType === 'bold')
        return (<strong>{outputElements}</strong>);
    if (token.emphasisType === 'italic')
        return (<em>{outputElements}</em>);
    return (<strong><em>{outputElements}</em></strong>);
};
