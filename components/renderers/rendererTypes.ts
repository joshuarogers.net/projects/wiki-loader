import { type Token } from "@/libs/tokens/tokenTypes";

export type TokenRendererOutput = JSX.Element | string | null;
export type TokenRenderer<T extends Token> = (token: T, renderToken: (token: Token) => TokenRendererOutput) => TokenRendererOutput;
