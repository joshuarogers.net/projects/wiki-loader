import styled from "@emotion/styled";
import { useContext } from "react";
import { ColorModeContext } from "./colorMode";

export const Logo = () => {
    const { isDarkMode } = useContext(ColorModeContext);

    const LogoImage = isDarkMode
        ? styled.img`filter: invert(1)`
        : styled.img();

    return (
        <LogoImage id="logo" src="logo.png" width="280" height="53.5" alt="Pathfinder logo. Link to home page." />
    );
};
