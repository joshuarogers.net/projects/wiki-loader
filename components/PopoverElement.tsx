import { Popover } from "@mui/material";
import { useState } from "react";

export interface PopoverElementProps {
    triggerComponent: (onTrigger: (target: Element) => void) => JSX.Element;
    popoverContent: (onClose: () => void) => JSX.Element;
}

export const PopoverElement = (props: PopoverElementProps) => {
    const [anchorElement, setAnchorElement] = useState<Element | null>(null);

    const onPopoverOpen = (element: Element) => setAnchorElement(anchorElement ? null : element);
    const onPopoverClose = () => setAnchorElement(null);

    return (<>
        {props.triggerComponent(onPopoverOpen)}
        <Popover open={anchorElement !== null} onClose={onPopoverClose} anchorEl={anchorElement} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
        }}>
            {props.popoverContent(onPopoverClose)}
        </Popover>
    </>);
};
