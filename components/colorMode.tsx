import { createTheme, CssBaseline, ThemeOptions, ThemeProvider, useMediaQuery } from "@mui/material";
import { createContext, useCallback, useEffect, useMemo, useState } from "react";

interface Palette {
    primary: string,
    secondary: string,
    heading: string,
    text: string,
    background: string,
    mode: 'light' | 'dark'
}

const lightModeColors: Palette = {
    primary: '#F50057',
    secondary: '#F50057',
    heading: '#FF8966',
    text: '#2A2B2A',
    background: '#FFFFFF',
    mode: 'light'
};

//https://coolors.co/2a2b2a-706c61-f8f4e3-e5446d-ff8966
const darkModeColors: Palette = {
    primary: '#FF8966',
    secondary: '#F50057',
    heading: '#FF8966',
    text: '#F8F4E3',
    background: '#2A2B2A',
    mode: 'dark'
};

const buildTheme = (palette: Palette): ThemeOptions => createTheme({
    palette: {
        primary: {
            main: palette.primary
        },
        secondary: {
            main: palette.secondary
        },
        text: {
            primary: palette.text
        },
        background: {
            default: palette.background
        },
        mode: palette.mode
    },
    typography: {
        h1: {
            color: palette.heading,
            fontSize: '3rem',
        },
        h2: {
            color: palette.heading,
            fontSize: '2.5rem',
        },
        h3: {
            color: palette.heading,
            fontSize: '2rem',
        },
        h4: {
            color: palette.heading,
            fontSize: '1.7rem',
        },
        h5: {
            color: palette.heading
        },
        h6: {
            color: palette.heading
        }
    }
});

export type SupportedColorMode = 'dark' | 'light';

export interface ColorModeContextValue {
    isDarkMode: boolean,
    setLightMode(): void,
    setDarkMode(): void
}

export const ColorModeContext = createContext<ColorModeContextValue>({ isDarkMode: false, setLightMode: () => {}, setDarkMode: () => {} });

export const ColorModeProvider = ({children, colorMode}: {children: JSX.Element, colorMode: 'light' | 'dark'}) => {
    const [isDarkMode, setIsDarkMode] = useState<boolean>(colorMode === 'dark');
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
    const theme = useMemo(() => buildTheme(isDarkMode ? darkModeColors : lightModeColors), [isDarkMode]);
    const setColorMode = useCallback((colorMode: 'light' | 'dark') => {
        setIsDarkMode(colorMode === 'dark');

        // @Todo: Set expiration and stuff
        document.cookie = `colorMode=${colorMode}`;
    }, []);

    useEffect(() => {
        if (document.cookie.indexOf("colorMode") > -1)
            return;
    
        setColorMode(prefersDarkMode ? "dark" : "light");
    }, []);

    const contextValue = useMemo<ColorModeContextValue>(() => ({
        isDarkMode,
        setLightMode: () => { setColorMode('light') },
        setDarkMode: () => { setColorMode('dark') }
    }), [isDarkMode]);

    return (
        <ColorModeContext.Provider value={contextValue}>
            <ThemeProvider theme={theme}>
                <CssBaseline enableColorScheme={true} />
                {children}
            </ThemeProvider>
        </ColorModeContext.Provider>
    )
};
