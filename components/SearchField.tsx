import { Autocomplete, TextField } from "@mui/material";
import { useState } from "react";

interface SearchResult {
    title: string;
}

interface SearchResults {
    results: SearchResult[];
}

export const searchByAlias = async (fetcher: typeof fetch, criteria: string): Promise<string[]> => {
    if (!criteria)
        return [];

    const request = await fetcher(`/api/autocomplete?q=${encodeURIComponent(criteria.trim())}`);
    const response = await request.json() as SearchResults;
    return response.results.map(x => x.title);
}

export interface SearchFieldSettings {
    onSelect: (text: string | null) => void;
    labelText: string;
    value: string | null;
    fetcher?: typeof fetch;
};

export const SearchField = (props: SearchFieldSettings) => {
    const [options, setOptions] = useState<string[]>([]);
    const fetcher = props.fetcher ?? fetch

    return (
        <Autocomplete
            sx={{ width: '100%' }}
            freeSolo
            options={options}
            onInputChange={async (_event, value) => {
                const result = await searchByAlias(fetcher, value);
                setOptions(result);
            }}
            onChange={(_event, value) => {
                // @Todo: Gross casting
                props.onSelect(value as string);
            }}
            filterOptions={(options) => options}
            value={props.value}
            autoHighlight={true}
            autoSelect={true}
            selectOnFocus={true}
            renderInput={(params) => <TextField {...params} label={props.labelText} />} />
    );
};

// @Todo: Why does "Maxwell Smart" return nothing?