import { useRouter } from "next/router";

export interface PathGenerationParameters {
    source?: string,
    destination?: string,
    pageId?: string
}

export interface PathfinderNavigation {
    generatePageUrl(parameters: PathGenerationParameters): string;
    pageId: string;
    source: string;
    destination: string;
}

export const usePathfinderNavigation = (): PathfinderNavigation => {
    const router = useRouter();
    const { pageId, source, destination } = router.query as Record<string, string>;

    return {
        generatePageUrl: ({pageId: newPageId, source: newSource, destination: newDestination}: PathGenerationParameters) =>
            `/path?source=${encodeURIComponent(newSource || source)}&destination=${encodeURIComponent(newDestination || destination)}&pageId=${encodeURIComponent(newPageId || pageId)}`,
        pageId: pageId || source,
        source,
        destination
    };
};
