import { Client as ElasticClient } from "@elastic/elasticsearch";
import { Client as PostgresClient } from "pg";
import Cursor from "pg-cursor";

import { config } from '@/libs/data/config';

const run = async () => {
    const client = new PostgresClient({
        host: config.postgresHost,
        port: config.postgresPort,
        user: config.postgresUser,
        password: config.postgresPass,
        database: config.postgresDatabase
    });
    await client.connect();
    
    const elasticSearchClient = new ElasticClient({ node: config.elasticSearchUri });
    await elasticSearchClient.indices.create({
        index: "wikipedia",
        mappings: {
            properties: {
                "page_id": {"type": "integer"},
                "aliases": {"type": "search_as_you_type"},
                "inbound_link_count": {"type": "integer"}
            }
        }
    });

    async function* iteratePostgresQuery(query: string) {
        const results = client.query(new Cursor(query));

        let x = 0;
        while (true) {
            const rows = await results.read(1000);
            if (rows.length === 0)
                break;

            for (const row of rows)
                yield row;

            x += rows.length;
            console.log(x);
        }
    }

    const pageAliasIterator = iteratePostgresQuery(`
        WITH alias AS (
            SELECT id as page_id, ARRAY_AGG(normalized_title) as aliases
            FROM page
            INNER JOIN page_alias pa
            ON page.id = pa.page_id
            GROUP BY id
        ),
        
        node_reference AS (
            SELECT target_pageid as page_id, COUNT(target_pageid) as incoming_count
            FROM page_link_internal
            GROUP BY target_pageid
        )
        
        SELECT alias.page_id, alias.aliases, coalesce(node_reference.incoming_count, 0) as inbound_link_count
        FROM alias
        LEFT OUTER JOIN node_reference
        ON (alias.page_id = node_reference.page_id)
    `);

    await elasticSearchClient.helpers.bulk({
        datasource: pageAliasIterator,
        onDocument: (doc) => ({ index: { _index: "wikipedia", _id: doc.page_id } }),
        concurrency: 1
    },
    {
        requestTimeout: 60000,
        maxRetries: 5
    });
};

run().then(() => process.exit(0));
