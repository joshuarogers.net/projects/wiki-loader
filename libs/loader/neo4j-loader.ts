import neo4j from "neo4j-driver";
import { Client as PostgresClient } from "pg";
import Cursor from "pg-cursor";

import { config } from "@/libs/data/config";

const run = async () => {
    const client = new PostgresClient({
        host: config.postgresHost,
        port: config.postgresPort,
        user: config.postgresUser,
        password: config.postgresPass,
        database: config.postgresDatabase
    });
    await client.connect();

    const neo4jDriver = neo4j.driver(config.neo4JUri,
        neo4j.auth.basic(config.neo4JUser, config.neo4JPass));

    async function* iteratePostgresQuery(query: string) {
        const results = client.query(new Cursor(query));

        let x = 0;
        while (true) {
            const rows = await results.read(1000);
            if (rows.length === 0)
                break;

            yield rows

            x += rows.length;
            console.log(x);
        }
    }

    await neo4jDriver.session().executeWrite(transaction => {
        transaction.run("CREATE CONSTRAINT page_id IF NOT EXISTS FOR (x:Page) REQUIRE x.id IS UNIQUE");
        transaction.run("CREATE CONSTRAINT page_title IF NOT EXISTS FOR (x:Page) REQUIRE x.title IS UNIQUE");
        transaction.run("CREATE INDEX page_incoming_links IF NOT EXISTS FOR (x:Page) ON (x.incoming_links)");
        transaction.run("CREATE INDEX page_outgoing_links IF NOT EXISTS FOR (x:Page) ON (x.outgoing_links)");
    });

    const pageSQL = `
    WITH incoming_link_metadata AS (
        SELECT target_pageid as page_id, COUNT(target_pageid) as link_count
        FROM page_link_internal
        GROUP BY target_pageid
    ), outgoing_link_metadata AS (
        SELECT source_pageid as page_id, COUNT(source_pageid) as link_count
        FROM page_link_internal
        GROUP BY source_pageid
    )
    
    SELECT id,
           title,
           COALESCE(incoming_link_metadata.link_count, 0) AS incoming_links,
           COALESCE(outgoing_link_metadata.link_count, 0) AS outgoing_links
    FROM page
    LEFT OUTER JOIN incoming_link_metadata
    ON page.id = incoming_link_metadata.page_id
    LEFT OUTER JOIN outgoing_link_metadata
    ON page.id = outgoing_link_metadata.page_id`;

    for await (const pages of iteratePostgresQuery(pageSQL)) {
        await neo4jDriver.session().run({
            text: `FOREACH (page in $pages |
                CREATE (:Page { id: page.id, title: page.title, incoming_links: page.incoming_links, outgoing_links: page.outgoing_links}))`,
            parameters: { pages }
        });
    }

    const pageLinksSQL = `SELECT * FROM page_link_internal`;

    for await (const pageLinks of iteratePostgresQuery(pageLinksSQL)) {
        await neo4jDriver.session().executeWrite(transaction => {
            transaction.run(`UNWIND $pageLinks as pageLink
                MATCH (source:Page { id: pageLink.source_pageid })
                MATCH (target:Page { id: pageLink.target_pageid })
                CREATE (source)-[:LINKS_TO]->(target)
            `, { pageLinks });
        });
    }
};

run().then(() => process.exit(0));
