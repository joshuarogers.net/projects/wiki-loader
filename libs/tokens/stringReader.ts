import { minBy } from "lodash";

// These aren't strictly required, but they make it much easier for me to ignore the exact
// manipulation of strings and indices and just focus on the description of the problem below.
// As-is, StringReader gives a simple way for me to say "give me all of the characters between
// here (wherever that is) and the index where this func becomes true/false."
export interface StringReader {
    checkpoint: (fn: () => void) => string;
    isEnd: () => boolean;
    peek: (length: number) => string;
    read: (length?: number) => string;
    readWhile: (fn: (char: string) => boolean) => string;
    readUntil: (readMode: 'discardMatch' | 'includeMatch' | 'stopBeforeMatch', ...stopWords: string[]) => string;
}

export const createStringReader = (source: string): StringReader => {
    let index = 0;

    const checkpoint = (fn: () => void): string => {
        const startIndex = index;
        fn();
        return source.slice(startIndex, index);
    };
    const isEnd = () => source.length <= index;
    const peek = (length: number) => source.slice(index, index + length);
    const read = (length: number = 1) => {
        const result = source.substring(index, index + length);
        index += length;
        return result;
    }

    return {
        checkpoint,
        isEnd,
        peek,
        read,
        readWhile: (fn: (char: string) => boolean) => checkpoint(() => {
            while (!isEnd() && fn(peek(1)))
                read();
        }),
        readUntil: (readMode: 'discardMatch' | 'includeMatch' | 'stopBeforeMatch', ...stopWords: string[]) => {
            const stopWordPositions = stopWords
                .map(stopWord => {
                    const stopWordIndex = source.indexOf(stopWord, index);
                    const stopWordStart = stopWordIndex >= 0
                        ? stopWordIndex
                        : source.length;
                    return { stopWordStart, stopWordEnd: stopWordStart + stopWord.length };
                });

            const nextStopWordPosition = minBy(stopWordPositions, x => x.stopWordStart)!;
            const body = checkpoint(() => {
                if (readMode === 'discardMatch' || readMode === 'stopBeforeMatch')
                    index = nextStopWordPosition.stopWordStart;
                else if (readMode === 'includeMatch')
                    index = nextStopWordPosition.stopWordEnd;
                else
                    throw new Error(`Unknown readMode: ${readMode}`);
            });

            index = readMode === 'stopBeforeMatch'
                ? nextStopWordPosition.stopWordStart
                : nextStopWordPosition.stopWordEnd;
            return body;
        }
    };
};

export const readBalancedPair = (stringReader: StringReader, openingCharacter: string, closingCharacter: string) => {
    return stringReader.checkpoint(() => {
        let openPairs = 0;
        do {
            const char = stringReader.read();
            if (char === openingCharacter)
                openPairs++;
            else if (char === closingCharacter)
                openPairs--;
        } while (openPairs > 0 && !stringReader.isEnd());
    });
};