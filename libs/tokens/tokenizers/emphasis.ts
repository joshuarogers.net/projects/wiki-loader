import type { StringReader } from "../stringReader";
import type { TextEmphasisType, Token, Tokenizer } from "../tokenTypes";

enum EmphasisTypes {
    'none' = 0,
    'italic' = 1,
    'bold' = 2,
    'bold/italic' = 3
}

const emphasisMarkerLengths: Record<number, EmphasisTypes> = {
    2: EmphasisTypes['italic'],
    3: EmphasisTypes['bold'],
    5: EmphasisTypes['bold/italic']
};

const readEmphasisMarker = (stringReader: StringReader): EmphasisTypes => {
    const markerLength = stringReader.readWhile(x => x === "'").length;
    return emphasisMarkerLengths[markerLength] || EmphasisTypes.none;
};

export const tokenizeEmphasis: Tokenizer = (stringReader, tokenize) => {
    // Various cases to keep in mind:
    //
    // This ''is italic'''''but now its bold'''
    // This ''is italic'''and bold'''''
    // This '''''is bold and italic'''and then italic''
    // This '''''is bold and italic''and then bold'''
    // This ''is italic'''and bold''and then only bold'''

    const tokens: Token[] = [];
    let currentEmphasis = EmphasisTypes.none;

    // The check for a newline character in this loop is a bit of a hack
    // simply to deal with cases that are still parsed incorrectly where
    // no stop is detected. If an emphasis stop is not detected, this at
    // least lets us limit the incorrect emphasis to a single paragraph.
    //
    // Example: https://en.wikipedia.org/w/index.php?title=Karen_McDougal&oldid=1094978374
    // starting with the phrase "first attempt at".
    while(!stringReader.isEnd() && stringReader.peek(1) !== '\n') {
        const emphasisMarker = readEmphasisMarker(stringReader);
        currentEmphasis ^= emphasisMarker;

        if (currentEmphasis === EmphasisTypes.none)
            break;

        const content = stringReader.readUntil('stopBeforeMatch', "''", '\n');
        tokens.push({
            'type': 'text-emphasis',
            'emphasisType': EmphasisTypes[currentEmphasis] as TextEmphasisType,
            'tokens': tokenize(content)
        });
    }

    return tokens;
};

export const tokenizeEmphasizedLine: Tokenizer = (stringReader, tokenize) => {
    stringReader.read(2);
    const tokens = tokenize(stringReader.readUntil('stopBeforeMatch', '\n'));
    return { type: 'text-emphasis', tokens, emphasisType: 'bold' };
}
