import { chain } from "lodash";
import { readBalancedPair } from "../stringReader";
import type { Tokenizer } from "../tokenTypes";

const extractKeyValuePairs = (input: string): [string, string] | null => {
    // Easiest way to split on only the first instance of a given character. Adapted from:
    // https://stackoverflow.com/questions/4607745/split-string-only-on-first-instance-of-specified-character
    const [key, value, _] = input.split(/=(.*)/);

    return value !== undefined
        ? [key.trim(), value.trim()]
        : null;
}

export const tokenizeTemplate: Tokenizer = (stringReader, tokenize) => {
    const body = readBalancedPair(stringReader, '{', '}');
    const [templateReference, ...templateArguments] = body
        .slice(2, -2)
        .split('\n|')
        .map(x => x.trim());

    const [templateType, templateSubtype, _] = templateReference?.split(/\s(.*)/) || [];
    if (templateType === null)
        return { type: 'unsupported' };

    if (templateType.toLowerCase() === 'infobox') {
        const keyValuePairs = chain(templateArguments)
            .map(extractKeyValuePairs)
            .compact()
            .map(([key, value]) => ({
                key,
                value: tokenize(value)
            })).value();

        return {
            type: 'infobox',
            infoBoxType: templateSubtype,
            values: keyValuePairs
        };
    }
    
    return { type: 'unsupported' };
};
