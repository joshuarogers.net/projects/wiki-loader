import { type Tokenizer } from "../tokenTypes";

export const tokenizeNewline: Tokenizer = stringReader => {
    stringReader.read();
    return { type: 'newline' };
};
