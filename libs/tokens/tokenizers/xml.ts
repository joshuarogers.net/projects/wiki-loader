import type { Tokenizer } from "../tokenTypes";

export const tokenizeXml: Tokenizer = stringReader => {
    const tagBody = stringReader.readUntil('includeMatch', '>');

    // An unexpected entry to the list, </noinclude>, is a special Wikipedia
    // directive that acts as a single tag but looks like a closing tag.
    const isSingleTag = tagBody.endsWith('/>')
        || tagBody.startsWith('<!--')
        || tagBody.startsWith('<br')
        || tagBody.startsWith('</');

    if (!isSingleTag) {
        const tagName = tagBody.split(/[ >]/)[0];
        const closingTag = `</${tagName.substring(1)}>`;
        stringReader.readUntil('discardMatch', closingTag);
    }

    return { type: 'unsupported' };
};