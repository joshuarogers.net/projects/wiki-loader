import { find, fromPairs, last } from "lodash";
import { createStringReader } from "../stringReader";
import type { TableCellAlignment, TableRow, Token, Tokenizer } from "../tokenTypes";

const parseSettings = (settingText: string) => {
    const settings = fromPairs(settingText
        .split(' ')
        .map(x => x.replaceAll('"', '').replaceAll("'", '').split('='))
        .filter(x => x.length === 2) as [string, string][]);

    const style = fromPairs(settings["style"]
        ?.split(";")
        .map(x => x.split(':').filter(x => x))
        .filter(x => x.length === 2));

    return {
        width: parseInt(settings['colspan']) || 1,
        height: parseInt(settings['rowspan']) || 1,
        alignment: (settings['align'] ?? style["text-align"]) as TableCellAlignment
    };
};

enum RowType {
    Header,
    Content
}

// I'm tempted to rewrite this NOT as a regex for readability. As is, the rather
// cryptic regex states the following: a string is a valid settings string IFF
// it takes the form of KEY=value, where key is constrained to alphanums and
// hyphen (such as 'col-span'), value is a single- or double-quoted string OR
// a single alphanum word and %, multiple key/value pairs are separated by one or
// more spaces, and the entire sequence is followed by a pipe. Additionally,
// spaces are allowed between every component.
const settingValidationRegex = /^(\s*[\w-]+\s*=\s*(".*?"|'.*?'|[\w%]+)\s*)+\|/i;
const isValidSettingsDirective = (potentialSettingText: string): boolean => settingValidationRegex.test(potentialSettingText);

// A caption is defined as the text after the first |+ until the end of the line.
const captionMatchRegex = /\|\+(.*)\n/;

const tokenizeRow = (rowContents: string, tokenize: (input: string) => Token[]): [RowType, TableRow] => {
    // The cleanest way to identify the start of a new cell is by a pipe or bang as the first
    // character on a line. This seems to work for all cases except the first item in each row
    // since the first is not preceeded by a new line. While a more correct way to resolve this
    // might have teen to match a bang and pipe alone on the first line, having special case
    // logic makes the whole thing more difficult to reason about. So, to address this, I
    // simply add a newline character before the data to be parsed ensuring that that special
    // case no longer exists: all cells, regardless of position, now start with a newline.
    const hackedRowContents = `\n${rowContents}`;
    const rowReader = createStringReader(hackedRowContents);
    const readToCellStart = () => rowReader.readUntil('stopBeforeMatch', '\n!', '\n|');
    const readRowType = () => rowReader.read(2) === '\n!' ? RowType.Header : RowType.Content;

    readToCellStart();
    const rowType = readRowType();

    const cells: TableRow = [];
    while (!rowReader.isEnd()) {
        const cellTagContents = readToCellStart();
        readRowType();

        const endOfStylingIndex = isValidSettingsDirective(cellTagContents)
            ? cellTagContents.indexOf('|')
            : -1;
        const styleContents = cellTagContents.substring(0, endOfStylingIndex);
        const displayContents = cellTagContents.substring(endOfStylingIndex + 1);

        const cellStyling = parseSettings(styleContents);
        const values = tokenize(displayContents);

        cells.push({ ...cellStyling, values });
    }

    return [rowType, cells];
};

export const tokenizeTable: Tokenizer = (stringReader, tokenize) => {
    stringReader.read(2);

    // Tables can be described either with one item per line, such as
    // | Value 1
    // | Value 2
    //
    // or
    // | Value 1 || Value 2
    //
    // While either type is valid, this makes parsing a bit more annoying. Rather than
    // making the parser support both methods, we can quickly turn the multiple-value
    // format into the single-value format by performing a few string replacements. This
    // lets us get away with only needing a single parser.
    const tableTagContents = stringReader
        .readUntil('discardMatch', '|}')
        .replaceAll('!!', '\n!')
        .replaceAll('||', '\n|')
        .trim();

    const captionlessTableTagContents = tableTagContents.replace(captionMatchRegex, "|-\n");
    const captionContents = last(tableTagContents.match(captionMatchRegex))?.trim() || null;
    const caption = captionContents
        ? tokenize(captionContents)
        : null;

    // Technically, the first row is after a "|-" but, as I found by stumbling across the page
    // for Hox Gene, Wikipedia is more than fine with treating the content after the first line.
    const startOfFirstRow = Math.max(captionlessTableTagContents.indexOf('\n'), 0);
    const rowContents = captionlessTableTagContents
        .substring(startOfFirstRow)
        .split(/\|[-+]/)
        .map(x => x.trim());

    const allRows = rowContents.map(x => tokenizeRow(x, tokenize));
    const headerRow = find(allRows, ([type, _row]) => type === RowType.Header)?.[1];
    const contentRows = allRows
        .filter(([type, _row]) => type === RowType.Content)
        .map(([_type, row]) => row);

    return {
        type: 'table',
        caption,
        header: headerRow || null,
        rows: contentRows
    };
};
