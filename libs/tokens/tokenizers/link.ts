import { last } from "lodash";
import { readBalancedPair } from "../stringReader";
import type { Tokenizer } from "../tokenTypes";

export const tokenizeInternalLink: Tokenizer = stringReader => {
    const completeTag = readBalancedPair(stringReader, '[', ']');
    const headerSize = 2;
    const tagContents = completeTag.slice(headerSize, -headerSize);

    const [destination, alias, ...rest] = tagContents.split('|');
    const page = destination.split('#')[0];

    if (rest.length === 0)
        return { type: 'link', page, alias: alias ?? page, relation: 'internal' };

    // https://en.wikipedia.org/wiki/Wikipedia:Images_linking_to_articles
    const isFile = destination.startsWith('File:') || destination.startsWith('Image:');
    if (isFile && (rest.length === 3 || rest.length === 4 || rest.length === 5)) {
        const fileId = destination.substring(destination.indexOf(':') + 1);
        const url = `https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/${encodeURI(fileId)}`;
        const fileExtension = last(url.split('.'))?.toLowerCase();

        if (fileExtension === 'webm' || fileExtension === 'mp4') {
            return {
                type: 'video',
                url,
                format: fileExtension
            };
        }

        return {
            type: 'image',
            url
        };
    }

    return { type: 'unsupported' };
};

export const tokenizeExternallLink: Tokenizer = stringReader => {
    const completeTag = readBalancedPair(stringReader, '[', ']');
    const headerSize = 1;
    const tagContents = completeTag.slice(headerSize, -headerSize);

    const [page, ...aliasParts] = tagContents.split(' ');

    return { type: 'link', page, alias: aliasParts.join(' '), relation: 'external' }
};