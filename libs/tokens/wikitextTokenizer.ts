import { createStringReader } from "./stringReader";
import { tokenizeEmphasis, tokenizeEmphasizedLine } from "./tokenizers/emphasis";
import { tokenizeHeading } from "./tokenizers/heading";
import { tokenizeExternallLink, tokenizeInternalLink } from "./tokenizers/link";
import { tokenizeList } from "./tokenizers/list";
import { tokenizeTemplate } from "./tokenizers/template";
import { tokenizeNewline } from "./tokenizers/newline";
import { tokenizeTable } from "./tokenizers/table";
import { tokenizeXml } from "./tokenizers/xml";
import { Token, Tokenizer } from "./tokenTypes";

const wikipediaTokenizers: Record<string, Tokenizer> = {
    'default': stringReader => ({
        type: 'text',
        value: stringReader.readWhile(x => wikipediaTokenizers[x] === undefined && wikipediaTokenizers[stringReader.peek(2)] === undefined)
    }),
    '\n': tokenizeNewline,
    '\n*': tokenizeList,
    '\n#': tokenizeList,
    '\n;': tokenizeEmphasizedLine,
    '{{': tokenizeTemplate,
    '==': tokenizeHeading,
    '[[': tokenizeInternalLink,
    '[': tokenizeExternallLink,
    "''": tokenizeEmphasis,
    '{|': tokenizeTable,
    '<': tokenizeXml
};

export const tokenize = (text: string): Token[] => {
    const stringReader = createStringReader(text
        .replaceAll('&nbsp;', ' ')
        .replaceAll('&#39;', "'"));
    const tokens: Token[] = [];

    while (!stringReader.isEnd()) {
        const tokenizer = wikipediaTokenizers[stringReader.peek(2)]
            || wikipediaTokenizers[stringReader.peek(1)]
            || wikipediaTokenizers['default'];

        const currentToken = tokenizer(stringReader, tokenize);
        tokens.push(...(Array.isArray(currentToken) ? currentToken : [currentToken]));
    }

    return tokens;
};
