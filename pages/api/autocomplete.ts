import { getElasticSearchClient } from "@/libs/data/elasticsearch";
import { getPostgresClient } from "@/libs/data/postgres";
import type { NextApiRequest, NextApiResponse } from 'next';
import type { Client as ElasticSearchClient } from "@elastic/elasticsearch";
import type { Client as PostgresClient } from "pg";
import { fromPairs, orderBy } from "lodash";

interface PageDocument {
    page_id: number
}

interface PageResult {
    _source: PageDocument
}

interface QueryResultSet {
    hits: QueryResult,
    took: number
}

interface QueryResult {
    hits: PageResult[]
}

const searchByPartialAlias = async (postgresClient: PostgresClient, elasticSearchClient: ElasticSearchClient, searchTerms: string): Promise<string[]> => {
    const result = await elasticSearchClient.search<QueryResultSet>({
        index: "wikipedia",
        body: {
            "_source": ["page_id"],
            "query": {
                "function_score": {
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "multi_match": {
                                        "fields": ["aliases", "aliases._2gram", "aliases._3gram"],
                                        "query": searchTerms,
                                        "type": "bool_prefix",
                                        "operator": "and"
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        }
    });

    const page_ids = result.hits.hits.map(x => (x as any)._source!["page_id"]);
    const results = await postgresClient.query<{title: string, id: number}>("SELECT title, id FROM page WHERE id = ANY($1)", [page_ids]);
    const titleByIdLookup = fromPairs(results.rows.map(x => [x.id, x.title]));

    return page_ids.map(x => titleByIdLookup[x]);
};

export default async function handler(request: NextApiRequest, response: NextApiResponse) {
    const postgresClient = await getPostgresClient();
    const elasticSearchClient = await getElasticSearchClient();

    const pageTitles = await searchByPartialAlias(postgresClient, elasticSearchClient, request.query.q as string);
    return response.json({
        results: pageTitles.map(x => ({ title: x }))
    });
};
