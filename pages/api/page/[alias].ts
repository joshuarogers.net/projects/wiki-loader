import { type Client as PostgresClient } from "pg";
import _ from "lodash";
import { normalizeAlias } from "@/utils";
import { getPostgresClient } from "@/libs/data/postgres";
import type { NextApiRequest, NextApiResponse } from 'next';

interface Page {
    alias: string,
    title: string,
    content: string
}

interface SQLitePage {
    alias: string,
    title: string,
    content: string
}

const getContentPage = async (postgresConnection: PostgresClient, alias: string): Promise<Page | {error: string}> => {
    const normalizedAlias = normalizeAlias(alias);
    const pageSql = `SELECT p.id, p.title, p.content, pa.normalized_title as alias
                     FROM page_alias pa
                     INNER JOIN page p
                       ON (pa.page_id = p.id)
                     WHERE pa.normalized_title = $1`;
    const row = await postgresConnection.query<SQLitePage>(pageSql, [normalizedAlias]);
    if (row.rowCount === 0)
        return {error: 'Page not found'};

    return row.rows[0];
};

const getCategoryPage = async (postgresConnection: PostgresClient, alias: string): Promise<Page | {error: string}> => {
    const normalizedAlias = normalizeAlias(alias);
    const pageTitles = await postgresConnection.query<{title: string}>(`SELECT p.title
        FROM category c
        INNER JOIN category_page cp
        ON c.id = cp.category_id
        INNER JOIN page p on cp.page_id = p.id
        WHERE c.title = $1
        ORDER BY p.title
        LIMIT 1000`, [normalizedAlias]);

    const parentTitles = await postgresConnection.query<{title: string}>(`SELECT parent.title
        FROM category parent
        INNER JOIN category_hierarchy ch
        ON parent.id = ch.parent_category_id
        INNER JOIN category child
        ON ch.child_category_id = child.id
        WHERE child.title = $1
        ORDER BY parent.title
        LIMIT 1000`, [normalizedAlias]);

    const subcategoryTitles = await postgresConnection.query<{title: string}>(`SELECT child.title
        FROM category parent
        INNER JOIN category_hierarchy ch
        ON parent.id = ch.parent_category_id
        INNER JOIN category child
        ON ch.child_category_id = child.id
        WHERE parent.title = $1
        ORDER BY child.title
        LIMIT 1000`, [normalizedAlias]);

    const hasMoreValues = pageTitles.rowCount === 1000 || parentTitles.rowCount === 1000 || subcategoryTitles.rowCount === 1000;
    let content = hasMoreValues
        ? "More results were found than can be displayed.\n\n"
        : '';

    if (parentTitles.rowCount)
        content += `===Parent categories===\n${parentTitles.rows.map(x => `* [[${x.title}|${x.title.replace('Category:', '')}]]`).join("\n")}\n\n`;
    if (subcategoryTitles.rowCount)
        content += `===Sub-categories===\n${subcategoryTitles.rows.map(x => `* [[${x.title}|${x.title.replace('Category:', '')}]]`).join("\n")}\n\n`;
    if (pageTitles.rowCount)
        content += `===Pages===\n${pageTitles.rows.map(x => `* [[${x.title}]]`).join("\n")}\n\n`;
    if (!content)
        content = "No category contents"

    return {
        title: normalizedAlias,
        alias: normalizedAlias,
        content: content
    };
};

export default async function handler(request: NextApiRequest, response: NextApiResponse) {
    const alias = request.query.alias as string;

    if (!alias)
        return response.json({error: 'No page alias was provided'});

    const postgresConnection = await getPostgresClient();

    const page = alias.startsWith('Category:')
        ? await getCategoryPage(postgresConnection, alias)
        : await getContentPage(postgresConnection, alias)

    return response.json(page);
}
