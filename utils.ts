export const normalizeAlias = (alias: string) => {
    const capitalizeFirstLetter = (x : string) => {
        return x.length >= 2
            ? x[0].toLocaleUpperCase() + x.substring(1)
            : x.toUpperCase();
    }

    const normalizedAlias = alias
        // _ and ' ' are equivalent
        .replace(/_/g, " ")
        // Multiple spaces in a row act as a single space
        .replace(/\s{2,}/g, ' ')
        // Leading and trailing whitespace do not count
        .trim()
        // Aliases starting with W: act the same as those that do not
        .replace(/^W:/i, '')
        // Aliases do not include # or any character after it
        .replace(/#.*$/, '');

    // @Todo: I feel like there are more rules here.
    return capitalizeFirstLetter(normalizedAlias);
};
