import { type VideoToken } from "../../src/tokens/tokenTypes";
import { tokenize } from "../../src/tokens/wikitextTokenizer";

describe("Video links", () => {
    test("can start with 'File:'", () => {
        const tokens = tokenize("[[File:Example2.webm|150px |link=Main Page |alt=Alt text |Title text]]");
        const expected: VideoToken = {
            type: 'video',
            url: 'https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Example2.webm',
            format: 'webm'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can start with 'Image:'", () => {
        const tokens = tokenize("[[Image:Example2.webm|150px |link=Main Page |alt=Alt text |Title text]]");
        const expected: VideoToken = {
            type: 'video',
            url: 'https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Example2.webm',
            format: 'webm'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can be 'webm' format", () => {
        const tokens = tokenize("[[Image:Example2.webm|150px |link=Main Page |alt=Alt text |Title text]]");
        const expected: VideoToken = {
            type: 'video',
            url: 'https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Example2.webm',
            format: 'webm'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can be 'mp4' format", () => {
        const tokens = tokenize("[[Image:Example2.mp4|150px |link=Main Page |alt=Alt text |Title text]]");
        const expected: VideoToken = {
            type: 'video',
            url: 'https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/Example2.mp4',
            format: 'mp4'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can appear midline", () => {
        const tokens = tokenize("Hello [[Image:Example2.mp4|150px |link=Main Page |alt=Alt text |Title text]] World");

        expect(tokens.map(x => x.type)).toEqual(["text", "video", "text"]);
    });
});

export {};
