import { LinkToken } from "../../src/tokens/tokenTypes";
import { tokenize } from "../../src/tokens/wikitextTokenizer";

describe("Internal links", () => {
    test("only require a page name", () => {
        const tokens = tokenize("[[Post Modern Jukebox]]");
        const expected: LinkToken = {
            type: 'link',
            alias: 'Post Modern Jukebox',
            page: 'Post Modern Jukebox',
            relation: 'internal'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can be aliased", () => {
        const tokens = tokenize("[[Post Modern Jukebox|PMJ]]");
        const expected: LinkToken = {
            type: 'link',
            alias: 'PMJ',
            page: 'Post Modern Jukebox',
            relation: 'internal'
        };

        expect(tokens).toEqual([expected]);
    });

    test("ignore anchors", () => {
        const tokens = tokenize("[[Post Modern Jukebox#History]]");
        const expected: LinkToken = {
            type: 'link',
            alias: 'Post Modern Jukebox',
            page: 'Post Modern Jukebox',
            relation: 'internal'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can appear midline", () => {
        const tokens = tokenize("Hello [[Example]] World");

        expect(tokens.map(x => x.type)).toEqual(["text", "link", "text"]);
    });
});

describe("External links", () => {
    test("can have multiword aliases", () => {
        const tokens = tokenize("[https://example.com Hello World]");
        const expected: LinkToken = {
            type: 'link',
            alias: 'Hello World',
            page: 'https://example.com',
            relation: 'external'
        };

        expect(tokens).toEqual([expected]);
    });

    test("can appear midline", () => {
        const tokens = tokenize("Hello [https://example.com Example] World");

        expect(tokens.map(x => x.type)).toEqual(["text", "link", "text"]);
    });
});

export {};
